// import fs from 'fs';

// import { initDbTables } from './dbUtils/initDbTables';
// import { analyzeFile } from './analyzer';
// import { generateFormatPlayerResultsQuery } from './analyzer/graphAnalyzer/queries';
// import { work_db } from './dbUtils/createConnections';

// (async () => {
//     await initDbTables();
//     const files = fs.readdirSync(process.env.MINING_DIR);
//     for (const file of files) {
//         await analyzeFile(`${process.env.MINING_DIR}/${file}`);
//     }

//     const formatePlayerResultsQuery = generateFormatPlayerResultsQuery();
//     await work_db.any(formatePlayerResultsQuery.query);

//     process.exit();
// })();

import {
    addAdditionalParams,
    calculateAdditionalStats,
} from './analyzer/agrAnalyzer/additionalResults';
import removeTempParams from './analyzer/agrAnalyzer/additionalResults/removeTempParams';
import {
    generateGetQuery,
    generateCreateQuery,
    generateUpdateQuery,
    generatePreCreateQuery,
    generateArgQuery,
} from './analyzer/agrAnalyzer/additionalResults/query';

require('dotenv').config();

const analyzer = require('hhp');

const fs = require('fs');
const dateFormat = require('dateformat');
const pgp = require('pg-promise')({});

const hm_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_HM2_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};

const work_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_WORK_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};

const postsRgx = /^([^:]+): posts small & big blinds [$|€]?([^ ]+)$/i;

// const capStr = `gordaopkbr: raises $4.75 to $5 and has reached the $5 cap
// capper: calls $4.75 and has reached the $5 cap`;
// const capLines = capStr.split(' cap');
// for (const line of capLines) {
//     const res = line.match(postsRgx);
//     console.log(res);
// }


const hm_db = pgp(hm_cn);
const work_db = pgp(work_cn);

function calculateRound(result, round, roundMark, sb, ante = 0) {
    for (const roundTurn of round) {
        switch (roundTurn.type) {
            case 'bet': {
                const raiseIndex = result.findIndex(x => x.player === roundTurn.player);
                if (raiseIndex === -1) {
                    console.log('raiseError');
                }
                if (raiseIndex !== -1) {
                    if (!result[raiseIndex].isCap) {
                        result[raiseIndex][roundMark] = -roundTurn.amount;
                    }
                    // result[raiseIndex][roundMark] = -roundTurn.amount;
                }

                break;
            }
            case 'fold':
                break;
            case 'raise': {
                const raiseIndex = result.findIndex(x => x.player === roundTurn.player);
                if (raiseIndex === -1) {
                    console.log('raiseError');
                }
                if (raiseIndex !== -1) {
                    if (!result[raiseIndex].isCap) {
                        if (roundMark === 'preflop' && result[raiseIndex].isPostsSbAndBB) {
                            result[raiseIndex][roundMark] = -roundTurn.raiseTo - sb - ante;
                        } else {
                            result[raiseIndex][roundMark] = -roundTurn.raiseTo - ante;
                        }
                    }
                    // result[raiseIndex][roundMark] = -roundTurn.raiseTo;
                }
                break;
            }
            case 'call': {
                const callIndex = result.findIndex(x => x.player === roundTurn.player);
                if (callIndex === -1) {
                    console.log('callError');
                }
                if (callIndex !== -1) {
                    if (!result[callIndex].isCap) {
                        result[callIndex][roundMark] -= roundTurn.amount;
                    }
                    // result[callIndex][roundMark] -= roundTurn.amount;
                }
                break;
            }
            case 'bet-returned': {
                const returnedIndex = result.findIndex(x => x.player === roundTurn.player);
                if (returnedIndex === -1) {
                    console.log('returnError');
                }
                if (returnedIndex !== -1) {
                    result[returnedIndex][roundMark] += roundTurn.amount;
                }
                break;
            }

            default:
        }
    }
    return result;
}

function calculateSummary(result, showdownInfo) {
    for (const showdownTurn of showdownInfo) {
        switch (showdownTurn.type) {
            case 'showed': {
                const collectIndex = result.findIndex(x => showdownTurn.player.indexOf(x.player) !== -1);
                if (showdownTurn.won) {
                    if (collectIndex === -1) {
                        console.log('collectError');
                    }
                    if (collectIndex !== -1) {
                        result[collectIndex].collect = showdownTurn.amount;
                    }
                }
                if (showdownTurn.description.indexOf('won') !== -1) {
                    const regExp = /(\d+\.?\d*)/;
                    const value = showdownTurn.description.match(regExp);
                    result[collectIndex].collect += parseFloat(value[0]);
                }
                break;
            }
            case 'collected': {
                const collectIndex = result.findIndex(x => showdownTurn.player.indexOf(x.player) !== -1);
                if (collectIndex === -1) {
                    console.log('collectError');
                }
                if (collectIndex !== -1) {
                    result[collectIndex].collect = showdownTurn.amount;
                }

                break;
            }
            default:
        }
    }
    return result;
}

function createTableInfo(info) {
    const date = new Date(info.year, info.month - 1, info.day, info.hour, info.min, info.sec);
    return {
        date,
        pokertype: info.pokertype,
        sb: info.sb,
        bb: info.bb,
        handid: info.handid,
    };
}

function createPlayerResults(result) {
    return result.map(x => ({
        player: x.player,
        res: x.collect + x.preflop + x.flop + x.turn + x.river + x.cap,
    }));
}

async function generateHandFromDb(res) {
    if (!res ||
        !res.table || !res.table.maxseats ||
        !res.info || !res.info.bb || !res.info.currency
    ) {
        return Promise.reject();
    }

    return {
        pokersite_id: 2,
        tablesize: res.table.maxseats,
        bigblindincents: res.info.bb * 100,
        pokergametype_id: res.info.cap ? 12 : 0,
        currencytype_id: res.info.currency === '$' ? 0 : 1,
    };
}

const s = 0;
const si = 0;


async function analyze(filename) {
    let errors = 0;
    // const txt = fs.readFileSync(filename).toString('utf8');
    // const test = txt.split('PokerStars Hand');
    // const startDate = new Date();
    // console.log('start: ', filename, startDate.getHours(), startDate.getMinutes());

    const limit = 1000;

    const lastHandFromFile = fs.readFileSync('lastHand.txt').toString('utf8');
    console.log('last handid from file', lastHandFromFile);

    const handsCount = await hm_db.one('select count(*) from handhistories where handhistory_id > $1', [+lastHandFromFile]);
    const chunksCount = Math.floor(handsCount.count / limit) + 1;

    console.log('new hands files count: ', handsCount);
    for (let i = 0; i < chunksCount; i += 1) {
        // console.log(i);

        const hands = await hm_db.many(`select handhistory from handhistories where handhistory_id > $1 order by handhistory_id limit ${limit} offset ${i * limit} `, [+lastHandFromFile])
            .catch(() => {
                console.log('Error: No hands in this chunck');
            });
        if (hands === undefined) {
            continue;
        }
        console.log(i * limit);
        for (const hand of hands) {
            const t = hand.handhistory;
            const res = analyzer.parseHand(hand.handhistory);
            if (res) {
                // console.log(res.info.handid);
                // if (res.seats.findIndex(x => x.player === 'b. illa 4:21') === -1) {
                //     continue;
                // }
                // if (res.info.handid !== '278300708') {
                //     continue;
                // }
                // if (res.info.handid !== '172987552551') {
                //     continue;
                // }
                // const index = t.indexOf('cap');
                // const tempString = t.substring(0, index);
                // console.log(tempString.split('\n').slice(-1)[0], test1);

                let result = res.seats.map(x => ({
                    player: x.player,
                    preflop: 0,
                    flop: 0,
                    turn: 0,
                    river: 0,
                    collect: 0,
                    cap: 0,
                    isCap: false,
                    isPostsSbAndBB: false, // this is when make cap
                }));


                // const postsSplit = t.split('posts small & big blinds');
                // const regexp = /\n([^:]+): posts small & big blinds [$|€]?([^ ]+)\n/i;
                // const postsSplit = t.match(regexp);

                // if (postsSplit) {
                //     console.log(postsSplit[1], postsSplit[2]);
                // }

                // for (let j = 0; j < postsSplit.length - 1; j += 1) {
                //     const postsBlock = postsSplit[j];
                //     console.log(postsBlock[1], postsBlock[2]);
                // }

                for (const post of res.posts) {
                    const postIndex = result.findIndex(x => x.player === post.player);
                    result[postIndex].preflop -= post.amount;
                    if (res.posts.indexOf(post) > 1 && post.type === 'sb') {
                        result[postIndex].isPostsSbAndBB = true;
                    }
                }

                let lines = t.split('\n');
                if (lines.length === 1) {
                    lines = t.split('\r\n');
                }

                for (const line of lines) {
                    const postsResult = line.match(postsRgx);
                    if (postsResult !== null) {
                        const postIndex = result.findIndex(x => x.player === postsResult[1]);
                        if (postIndex !== -1) {
                            result[postIndex].preflop -= postsResult[2];
                            result[postIndex].isPostsSbAndBB = true;
                        }
                    }
                }

                let capSplit = t.split(' cap\r\n');
                if (capSplit.length === 1) {
                    capSplit = t.split(' cap\n');
                }
                for (let j = 0; j < capSplit.length - 1; j += 1) {
                    const capBlock = capSplit[j];
                    let ls = capBlock.split('\r\n');
                    if (ls.length === 1) {
                        ls = capBlock.split('\n');
                    }
                    const capLine = ls.slice(-1)[0];
                    const words = capLine.split(/ bets | calls | raises | posts big blind /);

                    const capNick = words[0].substring(0, words[0].length - 1);
                    const capIndex = result.findIndex(x => capNick.indexOf(x.player) !== -1);
                    if (!words[1]) {
                        console.log('critical error');
                        continue;
                    }

                    const chipsBefore = res.seats.find(x => x.player === capNick).chips;

                    const capSplitString = words[1].split(' ');
                    let capAmount = capSplitString[capSplitString.length - 1].substring(1);

                    if (chipsBefore < capAmount) {
                        capAmount = chipsBefore;
                    }
                    if (capIndex !== -1) {
                        result[capIndex].cap = -capAmount;
                        result[capIndex].isCap = true;
                        result[capIndex].preflop = 0;

                        if (result[capIndex].isPostsSbAndBB) {
                            result[capIndex].cap -= res.info.sb;
                        }
                    }
                }

                //
                result = calculateRound(result, res.preflop, 'preflop', res.info.sb, res.info.ante);
                result = calculateRound(result, res.flop, 'flop');
                result = calculateRound(result, res.turn, 'turn');
                result = calculateRound(result, res.river, 'river');

                result = calculateSummary(result, res.summary);

                const filtered = res.summary.filter(x => x.metadata.raw.indexOf('won') !== -1 || x.metadata.raw.indexOf('collected') !== -1);


                if (process.env.ROOM === '888' || filtered.length === 0) {
                    const summary = t.split('*** SUMMARY ***')[1].split('\n');
                    for (const line of summary) {
                        // const r = line.match(/Seat\+([^:]+): (.+?) [$|€]?([^ ]+)/i);
                        // const r = line.match(/^Seat ([^:]+): (.+?) [$|€]?([^ ]+)$/i);
                        if (line.indexOf('won') !== -1 && line.indexOf('showed') !== -1) {
                            const playerIndex = result.findIndex(x => line.indexOf(x.player) !== -1);
                            const split = line.split(' ');
                            const sum = split[split.length - 1].slice(2, split[split.length - 1].length - 1);

                            if (playerIndex !== -1) {
                                result[playerIndex].collect = +sum;
                            }
                        }
                    }
                }


                const tableInfo = createTableInfo(res.info);


                const playerResults = createPlayerResults(result);


                const date = tableInfo.date.getTime();
                const formated = dateFormat(tableInfo.date, 'yyyymm');
                let promises = [];

                const handFromDb = await generateHandFromDb(res)
                    .catch(() => hm_db.one(`select * from handhistories
                        join gametypes on handhistories.gametype_id = gametypes.gametype_id
                        where gamenumber = $1`, [tableInfo.handid]))
                    .catch(() => {
                        console.log(`Error: No such hand in database ${tableInfo.handid}`);
                    });

                // const a = new Date();
                // const handFromDb = await hm_db.one(`select * from handhistories
                // join gametypes on handhistories.gametype_id = gametypes.gametype_id
                // where gamenumber = $1`, [tableInfo.handid])
                //     .catch(() => {
                //         console.log(`Error: No such hand in database ${tableInfo.handid}`);
                //     });
                // console.log(new Date().getTime() - a.getTime());

                if (!handFromDb) {
                    continue;
                }

                for (const playerRes of playerResults) {
                    // const playerFromGlobalPlayersDB =
                    //     await players_db.one('select * from players where playername = $1 and pokersite_id = $2', [playerRes.player, 2]);
                    const query = 'insert into playerresults (playername, pokersite_id, hand_id, result) values ($1, $2, $3, $4)';
                    promises.push(work_db.any(query, [playerRes.player, 2, tableInfo.handid, playerRes.res])
                        .catch((err) => {
                            console.log('Duplicate hand: ', res.info.handid);
                            errors += 1;
                        }));
                }


                promises.push(work_db.any(
                        `insert into hands
                    (hand_id, date, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize)
                    values ($1, $2, $3, $4, $5, $6, $7, $8)`,
                        [tableInfo.handid, date, formated, handFromDb.currencytype_id, handFromDb.pokergametype_id, handFromDb.pokersite_id,
                            handFromDb.bigblindincents, handFromDb.tablesize,
                        ],
                    )
                    .catch((err) => {
                        // console.log(err);
                    }));

                await Promise.all(promises);

                // let additionalParams = res.seats.map(x => ({
                //     player: x.player,
                // }));

                // const startAgr = new Date();

                let additionalParams = res.seats.map(x => ({
                    flopactionmadebeforecall: false,
                    flopactionmadebeforefold: false,
                    flopactionmadebeforeraise: false,
                    flopbetmade: false,
                    flopbetpossible: false,
                    flopcallmade: false,
                    flopcallpossible: false,
                    flopcallvsbetmade: false,
                    flopfoldvsbetmade: false,
                    flopraisemade: false,
                    flopraisepossible: false,
                    flopraisevsbetmade: false,
                    player: x.player,
                    riverraisemade: false,
                    riverraisepossible: false,
                    riverraisevsbetmade: false,
                    turnactionmadebeforecall: false,
                    turnactionmadebeforefold: false,
                    turnactionmadebeforeraise: false,
                    turnbetmade: false,
                    turnbetpossible: false,
                    turncallmade: false,
                    turncallpossible: false,
                    turncallvsbetmade: false,
                    turnfoldvsbetmade: false,
                    turnraisemade: false,
                    turnraisepossible: false,
                    turnraisevsbetmade: false,
                }));

                // additionalParams = addAdditionalParams(additionalParams);
                additionalParams = calculateAdditionalStats(additionalParams, res);
                additionalParams = removeTempParams(additionalParams);

                // console.log('agra 1:', new Date().getTime() - startAgr.getTime());

                // promises = [];
                // for (const additionalPlayerResult of additionalParams) {
                //     const updateQuery = generateUpdateQuery(additionalPlayerResult, handFromDb, formated);
                //     promises.push(work_db.one(updateQuery.query, updateQuery.data)
                //         // const a = new Date();
                //         // await work_db.one(updateQuery.query, updateQuery.data)
                //         .catch(async () => {
                //             const createQuery = generateCreateQuery(additionalPlayerResult, handFromDb, formated);
                //             await work_db.one(createQuery.query, createQuery.data)
                //                 .catch(() => {
                //                     console.log('ERROR KOTORII Y SLAVI');
                //                 });

                //             await work_db.one(updateQuery.query, updateQuery.data)
                //                 .catch(() => {
                //                     console.log('ERROR KOTORII Y SLAVI');
                //                 });
                //         }));
                //     // console.log(new Date().getTime() - a.getTime());
                // }

                // await Promise.all(promises);

                // s += new Date().getTime() - startAgr.getTime();
                // si += 1;

                // console.log(si, s / si);

                // console.log('agra 2:', new Date().getTime() - startAgr.getTime());

                // for (const additionalPlayerResult of additionalParams) {
                //     const getQuery = generateGetQuery(additionalPlayerResult, handFromDb, formated);

                //     await work_db.one(getQuery.query, getQuery.data)
                //         .catch(async () => {
                //             const createQuery = generateCreateQuery(additionalPlayerResult, handFromDb, formated);
                //             await work_db.any(createQuery.query, createQuery.data);
                //         });

                //     const updateQuery = generateUpdateQuery(additionalPlayerResult, handFromDb, formated);
                //     await work_db.any(updateQuery.query, updateQuery.data);
                // }

                promises = [];
                for (const additionalPlayerResult of additionalParams) {
                    const createPreQuery = generatePreCreateQuery(additionalPlayerResult, handFromDb, formated);
                    promises.push(work_db.any(createPreQuery.query, createPreQuery.data)
                        .catch(() => {}));
                }
                await Promise.all(promises);
            }
        }
    }

    const lastHandFromDb = await hm_db.one('select handhistory_id from handhistories order by handhistory_id desc limit 1');
    fs.writeFileSync('lastHand.txt', lastHandFromDb.handhistory_id);
    // for (let i = 0; i < test.length; i += 1) {
    //     const t = test[i];
    //     // const a = analyzer.canParse(`PokerStars ${t}`);
    //     const res = analyzer.parseHand(`PokerStars Hand${t}`);
    //     if (res) {
    //         // console.log(res.info.handid);
    //         // if (res.seats.findIndex(x => x.player === 'b. illa 4:21') === -1) {
    //         //     continue;
    //         // }
    //         // if (res.info.handid !== '278300708') {
    //         //     continue;
    //         // }
    //         // if (res.info.handid !== '172987552551') {
    //         //     continue;
    //         // }
    //         // const index = t.indexOf('cap');
    //         // const tempString = t.substring(0, index);
    //         // console.log(tempString.split('\n').slice(-1)[0], test1);

    //         let result = res.seats.map(x => ({
    //             player: x.player,
    //             preflop: 0,
    //             flop: 0,
    //             turn: 0,
    //             river: 0,
    //             collect: 0,
    //             cap: 0,
    //             isCap: false,
    //             isPostsSbAndBB: false, // this is when make cap
    //         }));


    //         // const postsSplit = t.split('posts small & big blinds');
    //         // const regexp = /\n([^:]+): posts small & big blinds [$|€]?([^ ]+)\n/i;
    //         // const postsSplit = t.match(regexp);

    //         // if (postsSplit) {
    //         //     console.log(postsSplit[1], postsSplit[2]);
    //         // }

    //         // for (let j = 0; j < postsSplit.length - 1; j += 1) {
    //         //     const postsBlock = postsSplit[j];
    //         //     console.log(postsBlock[1], postsBlock[2]);
    //         // }

    //         for (const post of res.posts) {
    //             const postIndex = result.findIndex(x => x.player === post.player);
    //             result[postIndex].preflop -= post.amount;
    //             if (res.posts.indexOf(post) > 1 && post.type === 'sb') {
    //                 result[postIndex].isPostsSbAndBB = true;
    //             }
    //         }

    //         let lines = t.split('\n');
    //         if (lines.length === 1) {
    //             lines = t.split('\r\n');
    //         }

    //         for (const line of lines) {
    //             const postsResult = line.match(postsRgx);
    //             if (postsResult !== null) {
    //                 const postIndex = result.findIndex(x => x.player === postsResult[1]);
    //                 if (postIndex !== -1) {
    //                     result[postIndex].preflop -= postsResult[2];
    //                     result[postIndex].isPostsSbAndBB = true;
    //                 }
    //             }
    //         }

    //         let capSplit = t.split(' cap\r\n');
    //         if (capSplit.length === 1) {
    //             capSplit = t.split(' cap\n');
    //         }
    //         for (let j = 0; j < capSplit.length - 1; j += 1) {
    //             const capBlock = capSplit[j];
    //             let ls = capBlock.split('\r\n');
    //             if (ls.length === 1) {
    //                 ls = capBlock.split('\n');
    //             }
    //             const capLine = ls.slice(-1)[0];
    //             const words = capLine.split(/ bets | calls | raises | posts big blind /);

    //             const capNick = words[0].substring(0, words[0].length - 1);
    //             const capIndex = result.findIndex(x => capNick.indexOf(x.player) !== -1);
    //             if (!words[1]) {
    //                 console.log('critical error');
    //                 continue;
    //             }

    //             const chipsBefore = res.seats.find(x => x.player === capNick).chips;

    //             const capSplitString = words[1].split(' ');
    //             let capAmount = capSplitString[capSplitString.length - 1].substring(1);

    //             if (chipsBefore < capAmount) {
    //                 capAmount = chipsBefore;
    //             }
    //             if (capIndex !== -1) {
    //                 result[capIndex].cap = -capAmount;
    //                 result[capIndex].isCap = true;
    //                 result[capIndex].preflop = 0;

    //                 if (result[capIndex].isPostsSbAndBB) {
    //                     result[capIndex].cap -= res.info.sb;
    //                 }
    //             }
    //         }

    //         //
    //         result = calculateRound(result, res.preflop, 'preflop', res.info.sb, res.info.ante);
    //         result = calculateRound(result, res.flop, 'flop');
    //         result = calculateRound(result, res.turn, 'turn');
    //         result = calculateRound(result, res.river, 'river');

    //         result = calculateSummary(result, res.summary);

    //         const filtered = res.summary.filter(x => x.metadata.raw.indexOf('won') !== -1 || x.metadata.raw.indexOf('collected') !== -1);


    //         if (process.env.ROOM === '888' || filtered.length === 0) {
    //             const summary = t.split('*** SUMMARY ***')[1].split('\n');
    //             for (const line of summary) {
    //                 // const r = line.match(/Seat\+([^:]+): (.+?) [$|€]?([^ ]+)/i);
    //                 // const r = line.match(/^Seat ([^:]+): (.+?) [$|€]?([^ ]+)$/i);
    //                 if (line.indexOf('won') !== -1 && line.indexOf('showed') !== -1) {
    //                     const playerIndex = result.findIndex(x => line.indexOf(x.player) !== -1);
    //                     const split = line.split(' ');
    //                     const sum = split[split.length - 1].slice(2, split[split.length - 1].length - 1);

    //                     if (playerIndex !== -1) {
    //                         result[playerIndex].collect = +sum;
    //                     }
    //                 }
    //             }
    //         }


    //         const tableInfo = createTableInfo(res.info);


    //         const playerResults = createPlayerResults(result);


    //         const date = tableInfo.date.getTime();
    //         const formated = dateFormat(tableInfo.date, 'yyyymm');
    //         let promises = [];

    //         const handFromDb = await generateHandFromDb(res)
    //             .catch(() => hm_db.one(`select * from handhistories
    //                 join gametypes on handhistories.gametype_id = gametypes.gametype_id
    //                 where gamenumber = $1`, [tableInfo.handid]))
    //             .catch(() => {
    //                 console.log(`Error: No such hand in database ${tableInfo.handid}`);
    //             });

    //         // const a = new Date();
    //         // const handFromDb = await hm_db.one(`select * from handhistories
    //         // join gametypes on handhistories.gametype_id = gametypes.gametype_id
    //         // where gamenumber = $1`, [tableInfo.handid])
    //         //     .catch(() => {
    //         //         console.log(`Error: No such hand in database ${tableInfo.handid}`);
    //         //     });
    //         // console.log(new Date().getTime() - a.getTime());

    //         if (!handFromDb) {
    //             continue;
    //         }

    //         for (const playerRes of playerResults) {
    //             // const playerFromGlobalPlayersDB =
    //             //     await players_db.one('select * from players where playername = $1 and pokersite_id = $2', [playerRes.player, 2]);
    //             const query = 'insert into playerresults (playername, pokersite_id, hand_id, result) values ($1, $2, $3, $4)';
    //             promises.push(work_db.any(query, [playerRes.player, 2, tableInfo.handid, playerRes.res])
    //                 .catch((err) => {
    //                     console.log('Duplicate hand: ', res.info.handid);
    //                     errors += 1;
    //                 }));
    //         }


    //         promises.push(work_db.any(
    //                 `insert into hands
    //             (hand_id, date, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize)
    //             values ($1, $2, $3, $4, $5, $6, $7, $8)`,
    //                 [tableInfo.handid, date, formated, handFromDb.currencytype_id, handFromDb.pokergametype_id, handFromDb.pokersite_id,
    //                     handFromDb.bigblindincents, handFromDb.tablesize,
    //                 ],
    //             )
    //             .catch((err) => {
    //                 // console.log(err);
    //             }));

    //         await Promise.all(promises);

    //         // let additionalParams = res.seats.map(x => ({
    //         //     player: x.player,
    //         // }));

    //         // const startAgr = new Date();

    //         let additionalParams = res.seats.map(x => ({
    //             flopactionmadebeforecall: false,
    //             flopactionmadebeforefold: false,
    //             flopactionmadebeforeraise: false,
    //             flopbetmade: false,
    //             flopbetpossible: false,
    //             flopcallmade: false,
    //             flopcallpossible: false,
    //             flopcallvsbetmade: false,
    //             flopfoldvsbetmade: false,
    //             flopraisemade: false,
    //             flopraisepossible: false,
    //             flopraisevsbetmade: false,
    //             player: x.player,
    //             riverraisemade: false,
    //             riverraisepossible: false,
    //             riverraisevsbetmade: false,
    //             turnactionmadebeforecall: false,
    //             turnactionmadebeforefold: false,
    //             turnactionmadebeforeraise: false,
    //             turnbetmade: false,
    //             turnbetpossible: false,
    //             turncallmade: false,
    //             turncallpossible: false,
    //             turncallvsbetmade: false,
    //             turnfoldvsbetmade: false,
    //             turnraisemade: false,
    //             turnraisepossible: false,
    //             turnraisevsbetmade: false,
    //         }));

    //         // additionalParams = addAdditionalParams(additionalParams);
    //         additionalParams = calculateAdditionalStats(additionalParams, res);
    //         additionalParams = removeTempParams(additionalParams);

    //         // console.log('agra 1:', new Date().getTime() - startAgr.getTime());

    //         // promises = [];
    //         // for (const additionalPlayerResult of additionalParams) {
    //         //     const updateQuery = generateUpdateQuery(additionalPlayerResult, handFromDb, formated);
    //         //     promises.push(work_db.one(updateQuery.query, updateQuery.data)
    //         //         // const a = new Date();
    //         //         // await work_db.one(updateQuery.query, updateQuery.data)
    //         //         .catch(async () => {
    //         //             const createQuery = generateCreateQuery(additionalPlayerResult, handFromDb, formated);
    //         //             await work_db.one(createQuery.query, createQuery.data)
    //         //                 .catch(() => {
    //         //                     console.log('ERROR KOTORII Y SLAVI');
    //         //                 });

    //         //             await work_db.one(updateQuery.query, updateQuery.data)
    //         //                 .catch(() => {
    //         //                     console.log('ERROR KOTORII Y SLAVI');
    //         //                 });
    //         //         }));
    //         //     // console.log(new Date().getTime() - a.getTime());
    //         // }

    //         // await Promise.all(promises);

    //         // s += new Date().getTime() - startAgr.getTime();
    //         // si += 1;

    //         // console.log(si, s / si);

    //         // console.log('agra 2:', new Date().getTime() - startAgr.getTime());

    //         // for (const additionalPlayerResult of additionalParams) {
    //         //     const getQuery = generateGetQuery(additionalPlayerResult, handFromDb, formated);

    //         //     await work_db.one(getQuery.query, getQuery.data)
    //         //         .catch(async () => {
    //         //             const createQuery = generateCreateQuery(additionalPlayerResult, handFromDb, formated);
    //         //             await work_db.any(createQuery.query, createQuery.data);
    //         //         });

    //         //     const updateQuery = generateUpdateQuery(additionalPlayerResult, handFromDb, formated);
    //         //     await work_db.any(updateQuery.query, updateQuery.data);
    //         // }

    //         promises = [];
    //         for (const additionalPlayerResult of additionalParams) {
    //             const createPreQuery = generatePreCreateQuery(additionalPlayerResult, handFromDb, formated);
    //             promises.push(work_db.any(createPreQuery.query, createPreQuery.data)
    //                 .catch(() => {}));
    //         }
    //         await Promise.all(promises);
    //     }
    // }
    return Promise.resolve();
}

const files = fs.readdirSync(process.env.MINING_DIR);

async function insertFormated() {
    await work_db.any(`INSERT INTO formatedplayerresults
            (playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id)(
            select playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id from (
           select playername, cast (rnum / 100 as int) as num, sum(result) as result, hm_format_date,
                currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize,  min(hand_id) as hand_id  from (
           select *, row_number() OVER () as rnum from (
           select playername, result, hm_format_date, currencytype_id, pokergametype_id, playerresults.pokersite_id, playerresults.hand_id,
                bigblindincents, tablesize from playerresults
           join hands on playerresults.hand_id = hands.hand_id order by playername, date ) as a) as b
           group by num, playername, hm_format_date, currencytype_id, pokergametype_id, pokersite_id,
                bigblindincents, tablesize )as c)`);

    // const players = await work_db.many('select playername, pokersite_id from playerresults group by playername, pokersite_id order by playername');

    // const playersCount = players.length;

    // for (let i = 0; i < playersCount; i += 1) {
    //     console.log('last operation player = ', players[i].playername, i + 1, '/', playersCount);
    //     await work_db.any(`INSERT INTO formatedplayerresults
    //             (playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id)(
    //             select playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id from (
    //         select playername, cast (rnum / 100 as int) as num, sum(result) as result, hm_format_date,
    //                 currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize,  min(hand_id) as hand_id  from (
    //         select *, row_number() OVER () as rnum from (
    //         select playername, result, hm_format_date, currencytype_id, pokergametype_id, playerresults.pokersite_id, playerresults.hand_id,
    //                 bigblindincents, tablesize from playerresults
    //         join hands on playerresults.hand_id = hands.hand_id
    //         where playername = $1 and playerresults.pokersite_id = $2
    //         order by playername, date ) as a) as b
    //         group by num, playername, hm_format_date, currencytype_id, pokergametype_id, pokersite_id,
    //                 bigblindincents, tablesize )as c)`, [players[i].playername, players[i].pokersite_id]);
    // }
}

async function insertAgr() {
    const createAgrQuery = generateArgQuery();
    await work_db.any(createAgrQuery.query);
}

async function start() {
    console.log('start');
    const sumTime = 0;
    const count = 0;

    const startTime = new Date().getTime();

    await analyze();

    const endTime = new Date().getTime();
    const diff = endTime - startTime;

    console.log(
        'time to this file:', (diff / 1000).toFixed(2), 'sec; speed:', (sumTime / count / 1000).toFixed(2),
        'sec/file; full time:', (sumTime / 1000).toFixed(2), 'sec; count:', count, 'files',
    );

    // for (const file of files) {
    //     const startTime = new Date().getTime();

    //     await analyze(`${process.env.MINING_DIR}/${file}`);

    //     const endTime = new Date().getTime();

    //     const diff = endTime - startTime;
    //     sumTime += diff;
    //     count += 1;
    //     console.log(
    //         'time to this file:', (diff / 1000).toFixed(2), 'sec; speed:', (sumTime / count / 1000).toFixed(2),
    //         'sec/file; full time:', (sumTime / 1000).toFixed(2), 'sec; count:', count, 'files',
    //     );
    // }

    await work_db.connect();
    await work_db.any('drop table if exists formatedplayerresults');

    await work_db.any(`
    CREATE TABLE formatedplayerresults (
        id serial,

        playername text,
        result double precision,
        hm_format_date bigint,
        currencytype_id smallint,
        pokergametype_id smallint,
        pokersite_id smallint,
        bigblindincents integer,
        tablesize smallint,
        hand_id bigint
    )`);

    await work_db.any('DROP table IF EXISTS additionalplayerresults cascade');
    await work_db.any(`create table additionalplayerresults (
        id serial,
        playername text,
        pokersite_id smallint,

        tablesize smallint,
        bigblindincents integer,
        playedyearandmonth integer,
        currencytype_id smallint,
        pokergametype_id smallint,

        flopbetmade integer default 0,
        flopbetpossible integer default 0,

        flopraisemade integer default 0,
        flopraisepossible integer default 0,

        flopcallmade integer default 0,
        flopcallpossible integer default 0,

        flopcallvsbetmade integer default 0,
        flopraisevsbetmade integer default 0,
        flopfoldvsbetmade integer default 0,

        turnbetmade integer default 0,
        turnbetpossible integer default 0,

        turnraisemade integer default 0,
        turnraisepossible integer default 0,

        turncallmade integer default 0,
        turncallpossible integer default 0,

        turncallvsbetmade integer default 0,
        turnraisevsbetmade integer default 0,
        turnfoldvsbetmade integer default 0,

        riverbetmade integer default 0,
        riverbetpossible integer default 0,

        riverraisemade integer default 0,
        riverraisepossible integer default 0,

        rivercallmade integer default 0,
        rivercallpossible integer default 0,

        rivercallvsbetmade integer default 0,
        riverraisevsbetmade integer default 0,
        riverfoldvsbetmade integer default 0
    )`).catch(() => {});

    await insertFormated();
    await insertAgr();

    let pokersite_id;
    switch (process.env.ROOM) {
        case ('888'):
            pokersite_id = 12;
            break;
        case ('CHI'):
            pokersite_id = 1;
            break;
        default:
            pokersite_id = 2;
    }

    console.log(process.env.ROOM, 'change id to:', pokersite_id);

    await work_db.any('update mycompiledplayerresults set pokersite_id = $1', [pokersite_id]);
    await work_db.any('update additionalplayerresults set pokersite_id = $1', [pokersite_id]);
    await work_db.any('update formatedplayerresults set pokersite_id = $1', [pokersite_id]);

    process.exit();
}

start();
