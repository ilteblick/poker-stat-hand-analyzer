import { hm_db, work_db } from '../dbUtils/createConnections';

async function start() {
    console.log('start testing results');
    const errors = [];
    await hm_db.connect();
    await work_db.connect();
    const playersFromHmDb = await work_db.many('select playername from mycompiledplayerresults group by playername');
    const playerFromWorkDb = await work_db.many('select playername from formatedplayerresults group by playername');

    if (playerFromWorkDb.length !== playersFromHmDb.length) {
        errors.push(`count players error - playersFromHmDb: ${playersFromHmDb.length}, playerFromWorkDb: ${playerFromWorkDb.length}`);
    }

    const countPlayers = playersFromHmDb.length;
    for (let i = 0; i < countPlayers; i += 1) {
        const player = playersFromHmDb[i];
        const playername = player.playername;

        console.log(`${i} / ${countPlayers} - ${playername}`);

        const resultFromHmDb = await work_db.one(`select sum(totalamountwonincents) as wins, sum(totalhands) 
            as hands from mycompiledplayerresults where playername = $1`, [playername]);
        const resultFromWorkDb = await work_db.one(`select count(*), sum(result) as wins
            from formatedplayerresults where playername = $1`, [playername]);

        const winFromHmDb = +resultFromHmDb.wins / 100;
        const winFromWorkDb = resultFromWorkDb.wins;
        const allowableError = winFromHmDb !== 0 ? winFromHmDb / 50 : 1;

        // if (+resultFromHmDb.hands !== +resultFromWorkDb.count) {
        //     errors.push(`hands error - playername: ${playername}`);
        // }

        if (winFromHmDb !== winFromWorkDb) {
            if (Math.abs(winFromHmDb - winFromWorkDb) > Math.abs(allowableError)) {
                errors.push(`wins error - playername: ${playername}, winFromHmDb: ${winFromHmDb}, winFromWorkDb: ${winFromWorkDb} on  ${resultFromHmDb.hands} hands`); // eslint-disable-line
            }
        }

        if (+resultFromHmDb.hands - (+resultFromWorkDb.count * 100) > 100) {
            errors.push(`wins error - playername: ${playername}, handsFromHmDb: ${resultFromHmDb.hands}, hadnsFromWorkDb: ${resultFromWorkDb.count * 100} `); // eslint-disable-line
        }
    }
    if (errors.length === 0) {
        console.log('All ok');
    } else {
        console.log('errors:', errors);
    }
    process.exit();
}

start();
