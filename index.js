require('babel-register');
require('dotenv').config();

if (process.argv.slice(2).indexOf('--test') === -1) {
    require('./server'); // eslint-disable-line
} else {
    require('./test'); // eslint-disable-line
}
