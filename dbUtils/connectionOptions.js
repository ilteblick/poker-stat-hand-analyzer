export const hm_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_HM2_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};

export const work_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_WORK_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};
