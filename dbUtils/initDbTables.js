import { hm_db, work_db } from './createConnections';

export async function initDbTables() {
    await hm_db.connect();
    await work_db.connect();

    await work_db.any('drop table if exists formatedplayerresults');

    await work_db.any(`
    CREATE TABLE formatedplayerresults (
        id serial,

        playername text,
        result double precision,
        hm_format_date bigint,
        currencytype_id smallint,
        pokergametype_id smallint,
        pokersite_id smallint,
        bigblindincents integer,
        tablesize smallint,
        hand_id bigint
    )`);
}
