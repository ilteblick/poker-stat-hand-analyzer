import { hm_cn, work_cn } from './connectionOptions';

const pgp = require('pg-promise')({});

export const hm_db = pgp(hm_cn);
export const work_db = pgp(work_cn);
