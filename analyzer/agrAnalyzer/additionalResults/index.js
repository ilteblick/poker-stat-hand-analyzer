import { calculateAllRoundStats } from './calculateRoundStats';
import addParams from './addParams';

export function calculateAdditionalStats(result, res) {
    return calculateAllRoundStats(result, res);
}

export function addAdditionalParams(result) {
    return addParams(result);
}
