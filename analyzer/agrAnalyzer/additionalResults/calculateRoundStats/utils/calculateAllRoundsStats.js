export function calculateAllRoundsStats(result, res, func) {
    result = func(result, res.flop, 'flop');
    result = func(result, res.turn, 'turn');
    result = func(result, res.river, 'river');

    return result;
}
