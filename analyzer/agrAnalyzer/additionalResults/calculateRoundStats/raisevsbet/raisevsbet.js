export function calculateRaiseVsBet(result, round, roundStr) {
    const actionmadebeforeraise = `${roundStr}actionmadebeforeraise`;
    const raisevsbet = `${roundStr}raisevsbetmade`;

    for (const roundTurn of round) {
        const playerIndex = result.findIndex(x => roundTurn.player.indexOf(x.player) !== -1);
        if (roundTurn.type === 'check' || roundTurn.type === 'bet' || roundTurn.type === 'call') {
            result[playerIndex][actionmadebeforeraise] = true;
        } else if (!result[playerIndex][actionmadebeforeraise] && roundTurn.type === 'raise') {
            result[playerIndex][raisevsbet] = true;
        }
    }

    return result;
}

