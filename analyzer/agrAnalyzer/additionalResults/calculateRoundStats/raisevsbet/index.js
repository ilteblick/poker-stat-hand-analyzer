import { calculateRaiseVsBet } from './raisevsbet';
import { calculateAllRoundsStats } from '../utils';

export default function calculateAllRoundsRaiseVsBet(result, res) {
    return calculateAllRoundsStats(result, res, calculateRaiseVsBet);
}
