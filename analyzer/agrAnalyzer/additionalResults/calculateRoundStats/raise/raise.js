export function calculateRaise(result, round, roundStr) {
    const raisemade = `${roundStr}raisemade`;
    const raisepossible = `${roundStr}raisepossible`;

    let isWasBet = false;
    let betPlayerNick;
    for (const roundTurn of round) {
        const playerIndex = result.findIndex(x => roundTurn.player.indexOf(x.player) !== -1);
        if (isWasBet && betPlayerNick !== roundTurn.player) {
            if (roundTurn.type === 'raise' || roundTurn.type === 'call' || roundTurn.type === 'fold') {
                result[playerIndex][raisepossible] = true;
            }

            if (roundTurn.type === 'raise') {
                result[playerIndex][raisemade] = true;
            }
        }

        if (roundTurn.type === 'bet') {
            isWasBet = true;
            betPlayerNick = roundTurn.player;
        }
    }

    return result;
}
