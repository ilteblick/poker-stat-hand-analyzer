import { calculateRaise } from './raise';
import { calculateAllRoundsStats } from '../utils';

export default function calculateAllRoundsRaise(result, res) {
    return calculateAllRoundsStats(result, res, calculateRaise);
}
