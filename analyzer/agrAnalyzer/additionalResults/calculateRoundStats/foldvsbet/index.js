import { calculateFoldVsBet } from './foldvsbet';
import { calculateAllRoundsStats } from '../utils';

export default function calculateAllRoundsFoldVsBet(result, res) {
    return calculateAllRoundsStats(result, res, calculateFoldVsBet);
}
