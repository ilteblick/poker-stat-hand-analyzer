export function calculateFoldVsBet(result, round, roundStr) {
    const actionmadebeforefold = `${roundStr}actionmadebeforefold`;
    const foldvsbet = `${roundStr}foldvsbetmade`;

    for (const roundTurn of round) {
        const playerIndex = result.findIndex(x => roundTurn.player.indexOf(x.player) !== -1);
        if (roundTurn.type === 'check' || roundTurn.type === 'bet' || roundTurn.type === 'call' || roundTurn.type === 'raise') {
            result[playerIndex][actionmadebeforefold] = true;
        } else if (!result[playerIndex][actionmadebeforefold] && roundTurn.type === 'fold') {
            result[playerIndex][foldvsbet] = true;
        }
    }

    return result;
}
