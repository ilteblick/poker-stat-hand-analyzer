export function calculateCall(result, round, roundStr) {
    const callmade = `${roundStr}callmade`;
    const callpossible = `${roundStr}callpossible`;

    let isWasBet = false;
    let betPlayerNick;
    for (const roundTurn of round) {
        const playerIndex = result.findIndex(x => roundTurn.player.indexOf(x.player) !== -1);
        if (isWasBet && betPlayerNick !== roundTurn.player) {
            if (roundTurn.type === 'raise' || roundTurn.type === 'call' || roundTurn.type === 'fold') {
                result[playerIndex][callpossible] = true;
            }

            if (roundTurn.type === 'call') {
                result[playerIndex][callmade] = true;
            }
        }

        if (roundTurn.type === 'bet') {
            isWasBet = true;
            betPlayerNick = roundTurn.player;
        }
    }

    return result;
}
