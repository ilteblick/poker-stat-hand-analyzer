import { calculateCall } from './call';
import { calculateAllRoundsStats } from '../utils';

export default function calculateAllRoundsCall(result, res) {
    return calculateAllRoundsStats(result, res, calculateCall);
}
