import calculateAllRoundsBet from './bet';
import calculateAllRoundsCall from './call';
import calculateAllRoundsRaise from './raise';
import calculateAllRoundsCallVsBet from './callvsbet';
import calculateAllRoundsRaiseVsBet from './raisevsbet';
import calculateAllRoundsFoldVsBet from './foldvsbet';

export function calculateAllRoundStats(result, res) {
    result = calculateAllRoundsBet(result, res);
    result = calculateAllRoundsCall(result, res);
    result = calculateAllRoundsRaise(result, res);
    result = calculateAllRoundsCallVsBet(result, res);
    result = calculateAllRoundsRaiseVsBet(result, res);
    result = calculateAllRoundsFoldVsBet(result, res);

    return result;
}
