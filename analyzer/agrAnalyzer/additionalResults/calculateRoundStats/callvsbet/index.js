import { calculateCallVsBet } from './callvsbet';
import { calculateAllRoundsStats } from '../utils';

export default function calculateAllRoundsCallVsBet(result, res) {
    return calculateAllRoundsStats(result, res, calculateCallVsBet);
}
