export function calculateCallVsBet(result, round, roundStr) {
    const actionmadebeforecall = `${roundStr}actionmadebeforecall`;
    const callvsbet = `${roundStr}callvsbetmade`;

    for (const roundTurn of round) {
        const playerIndex = result.findIndex(x => roundTurn.player.indexOf(x.player) !== -1);
        if (roundTurn.type === 'check' || roundTurn.type === 'bet' || roundTurn.type === 'raise') {
            result[playerIndex][actionmadebeforecall] = true;
        } else if (!result[playerIndex][actionmadebeforecall] && roundTurn.type === 'call') {
            result[playerIndex][callvsbet] = true;
        }
    }

    return result;
}
