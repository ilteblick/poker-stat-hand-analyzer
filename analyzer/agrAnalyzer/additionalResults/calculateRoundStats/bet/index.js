import { calculateBet } from './bet';
import { calculateAllRoundsStats } from '../utils';

export default function calculateAllRoundsBet(result, res) {
    return calculateAllRoundsStats(result, res, calculateBet);
}
