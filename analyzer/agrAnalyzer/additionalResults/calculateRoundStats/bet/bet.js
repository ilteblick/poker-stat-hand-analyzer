export function calculateBet(result, round, roundStr) {
    const betmade = `${roundStr}betmade`;
    const betpossible = `${roundStr}betpossible`;

    for (const roundTurn of round) {
        const playerIndex = result.findIndex(x => roundTurn.player.indexOf(x.player) !== -1);
        if (roundTurn.type === 'bet') {
            result[playerIndex][betmade] = true;
            result[playerIndex][betpossible] = true;
            break;
        } else {
            result[playerIndex][betpossible] = true;
        }
    }

    return result;
}
