import { removeAllRoundsCustomParams } from './removeTempParams';

export default function removeTempParams(result) {
    result = removeAllRoundsCustomParams(result, 'actionmadebeforecall');
    result = removeAllRoundsCustomParams(result, 'actionmadebeforeraise');
    result = removeAllRoundsCustomParams(result, 'actionmadebeforefold');


    return result;
}
