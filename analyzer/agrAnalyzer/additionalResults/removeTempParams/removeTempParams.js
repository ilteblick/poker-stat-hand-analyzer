export function removeAllRoundsCustomParams(result, param) {
    return result.map((x) => {
        delete x[`flop${param}`];
        delete x[`turn${param}`];
        delete x[`river${param}`];

        return x;
    });
}
