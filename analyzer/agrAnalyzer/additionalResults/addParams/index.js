import { addAllRoundsBoolParams, addAllRoundsCustomParams } from './addParams';

export default function addParams(result) {
    result = addAllRoundsBoolParams(result, 'bet', true);
    result = addAllRoundsBoolParams(result, 'raise', true);
    result = addAllRoundsBoolParams(result, 'call', true);

    result = addAllRoundsBoolParams(result, 'callvsbet');
    result = addAllRoundsCustomParams(result, 'actionmadebeforecall');

    result = addAllRoundsBoolParams(result, 'raisevsbet');
    result = addAllRoundsCustomParams(result, 'actionmadebeforeraise');

    result = addAllRoundsBoolParams(result, 'foldvsbet');
    result = addAllRoundsCustomParams(result, 'actionmadebeforefold');

    return result;
}
