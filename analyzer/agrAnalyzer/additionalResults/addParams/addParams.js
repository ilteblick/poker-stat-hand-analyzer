export function addAllRoundsBoolParams(result, param, addPossible = false) {
    let paramToAdd = {
        [`flop${param}made`]: false,
        [`turn${param}made`]: false,
        [`river${param}made`]: false,
    };

    if (addPossible) {
        const possibleParamsToAdd = {
            [`flop${param}possible`]: false,
            [`turn${param}possible`]: false,
            [`river${param}possible`]: false,
        };
        paramToAdd = Object.assign({}, paramToAdd, possibleParamsToAdd);
    }


    return result.map(x => Object.assign({}, x, paramToAdd));
}

export function addAllRoundsCustomParams(result, param) {
    const paramToAdd = {
        [`flop${param}`]: false,
        [`turn${param}`]: false,
        [`river${param}`]: false,
    };

    return result.map(x => Object.assign({}, x, paramToAdd));
}
