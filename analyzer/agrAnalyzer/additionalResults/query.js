export function generateGetQuery(additionalPlayerResult, handFromDb, date) {
    return {
        query: `select * from additionalplayerresults where 
            playername = $1 and
            pokersite_id = $2 and 
            tablesize = $3 and
            bigblindincents = $4 and
            playedyearandmonth = $5 and 
            currencytype_id = $6 and
            pokergametype_id = $7
            `,
        data: [
            additionalPlayerResult.player,
            handFromDb.pokersite_id,
            handFromDb.tablesize,
            handFromDb.bigblindincents,
            date,
            handFromDb.currencytype_id,
            handFromDb.pokergametype_id,
        ],
    };
}

export function generateCreateQuery(additionalPlayerResult, handFromDb, date) {
    return {
        query: `insert into additionalplayerresults  
            ( playername,
            pokersite_id, 
            tablesize,
            bigblindincents,
            playedyearandmonth,
            currencytype_id,
            pokergametype_id ) values ($1, $2, $3, $4, $5, $6, $7)

            RETURNING playername
            `,
        data: [
            additionalPlayerResult.player,
            handFromDb.pokersite_id,
            handFromDb.tablesize,
            handFromDb.bigblindincents,
            date,
            handFromDb.currencytype_id,
            handFromDb.pokergametype_id,
        ],
    };
}

export function generateUpdateQuery(additionalPlayerResult, handFromDb, date) {
    let updateQuery = '';
    for (const key in additionalPlayerResult) {
        if (key !== 'player') {
            if (additionalPlayerResult[key]) {
                updateQuery = `${updateQuery} ${key} = ${key} + 1,`;
            } else {
                updateQuery = `${updateQuery} ${key} = ${key},`;
            }
        }
    }

    return {
        query: ` update additionalplayerresults set ${updateQuery} playername = $1
        where 
            playername = $1 and
            pokersite_id = $2 and 
            tablesize = $3 and
            bigblindincents = $4 and
            playedyearandmonth = $5 and 
            currencytype_id = $6 and
            pokergametype_id = $7

            RETURNING playername
            `,
        data: [
            additionalPlayerResult.player,
            handFromDb.pokersite_id,
            handFromDb.tablesize,
            handFromDb.bigblindincents,
            date,
            handFromDb.currencytype_id,
            handFromDb.pokergametype_id,
        ],
    };
}

export function generatePreCreateQuery(additionalPlayerResult, handFromDb, date) {
    let fields = '';
    let values = '';
    for (const key in additionalPlayerResult) {
        if (key !== 'player') {
            fields = `${fields} ${key},`;
            if (additionalPlayerResult[key]) {
                values = `${values} 1,`;
            } else {
                values = `${values} 0,`;
            }
        }
    }

    return {
        query: ` insert into preadditionalplayerresults
                ( ${fields} playername,
                pokersite_id, 
                tablesize,
                bigblindincents,
                playedyearandmonth,
                currencytype_id,
                pokergametype_id ) values 
                (${values} $1, $2, $3, $4, $5, $6, $7)
            `,
        data: [
            additionalPlayerResult.player,
            handFromDb.pokersite_id,
            handFromDb.tablesize,
            handFromDb.bigblindincents,
            date,
            handFromDb.currencytype_id,
            handFromDb.pokergametype_id,
        ],
    };
}

export function generateArgQuery() {
    const query = `insert into additionalplayerresults 
    (
        playername ,
        pokersite_id ,

        tablesize ,
        bigblindincents ,
        playedyearandmonth ,
        currencytype_id ,
        pokergametype_id ,

        flopbetmade ,
        flopbetpossible ,

        flopraisemade ,
        flopraisepossible ,

        flopcallmade ,
        flopcallpossible ,

        flopcallvsbetmade ,
        flopraisevsbetmade ,
        flopfoldvsbetmade ,

        turnbetmade ,
        turnbetpossible ,

        turnraisemade ,
        turnraisepossible ,

        turncallmade ,
        turncallpossible ,

        turncallvsbetmade ,
        turnraisevsbetmade ,
        turnfoldvsbetmade ,

        riverbetmade ,
        riverbetpossible ,

        riverraisemade ,
        riverraisepossible ,

        rivercallmade ,
        rivercallpossible ,

        rivercallvsbetmade ,
        riverraisevsbetmade ,
        riverfoldvsbetmade )
    (select 
        playername,
        pokersite_id,
        tablesize,
        bigblindincents,
        playedyearandmonth,
        currencytype_id,
        pokergametype_id,

        sum(flopbetmade) as flopbetmade,
        sum(flopbetpossible) as flopbetpossible,

        sum(flopraisemade) as flopraisemade,
        sum(flopraisepossible) as flopraisepossible,

        sum(flopcallmade) as flopcallmade,
        sum(flopcallpossible) as flopcallpossible,

        sum(flopcallvsbetmade) as flopcallvsbetmade,
        sum(flopraisevsbetmade) as flopraisevsbetmade,
        sum(flopfoldvsbetmade) as flopfoldvsbetmade,

        sum(turnbetmade) as turnbetmade,
        sum(turnbetpossible) as turnbetpossible,

        sum(turnraisemade) as turnraisemade,
        sum(turnraisepossible) as turnraisepossible,

        sum(turncallmade) as turncallmade,
        sum(turncallpossible) as turncallpossible,

        sum(turncallvsbetmade) as turncallvsbetmade,
        sum(turnraisevsbetmade) as turnraisevsbetmade,
        sum(turnfoldvsbetmade) as turnfoldvsbetmade,

        sum(riverbetmade) as riverbetmade,
        sum(riverbetpossible) as riverbetpossible,

        sum(riverraisemade) as riverraisemade,
        sum(riverraisepossible) as riverraisepossible,

        sum(rivercallmade) as rivercallmade,
        sum(rivercallpossible) as rivercallpossible,

        sum(rivercallvsbetmade) as rivercallvsbetmade,
        sum(riverraisevsbetmade) as riverraisevsbetmade,
        sum(riverfoldvsbetmade) as riverfoldvsbetmade

        from preadditionalplayerresults

        group by playername,
            pokersite_id,
            tablesize,
            bigblindincents,
            playedyearandmonth,
            currencytype_id,
            pokergametype_id
    )`;

    return {
        query,
    };
}
