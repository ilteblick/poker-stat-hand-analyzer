import dateFormat from 'dateformat';
import { addAdditionalParams, calculateAdditionalStats } from './additionalResults';
import removeTempParams from './additionalResults/removeTempParams';
import { generateGetQuery, generateCreateQuery, generateUpdateQuery } from './additionalResults/query';
import { work_db } from '../../dbUtils/createConnections';

export async function agrAnalyzeFile(parsedHand, handFromHm, tableInfo) {
    const formated = dateFormat(tableInfo.date, 'yyyymm');

    let additionalParams = parsedHand.seats.map(x => ({
        player: x.player,
    }));

    additionalParams = addAdditionalParams(additionalParams);
    additionalParams = calculateAdditionalStats(additionalParams, parsedHand);
    additionalParams = removeTempParams(additionalParams);

    for (const additionalPlayerResult of additionalParams) {
        const getQuery = generateGetQuery(additionalPlayerResult, handFromHm, formated);

        await work_db.one(getQuery.query, getQuery.data)
            .catch(async () => {
                const createQuery = generateCreateQuery(additionalPlayerResult, handFromHm, formated);
                await work_db.any(createQuery.query, createQuery.data)
                    .catch(() => { });
            });

        const updateQuery = generateUpdateQuery(additionalPlayerResult, handFromHm, formated);
        await work_db.any(updateQuery.query, updateQuery.data)
            .catch(() => { });
    }
}
