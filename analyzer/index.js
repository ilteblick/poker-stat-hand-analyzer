import fs from 'fs';
import analyzer from 'hhp';
import { handAnalyzeFile } from './graphAnalyzer';
import { createTableInfo } from './graphAnalyzer/utils';
import { generateGetHandFromHm } from './graphAnalyzer/queries';
import { hm_db } from '../dbUtils/createConnections';
import { agrAnalyzeFile } from './agrAnalyzer';

export async function analyzeFile(filename) {
    const startDate = new Date();
    console.log('start analize file: ', filename, startDate.getHours(), startDate.getMinutes());

    const file = fs.readFileSync(filename).toString('utf8');
    const splitByPokerStars = file.split('PokerStars ');

    for (let i = 0; i < splitByPokerStars.length; i += 1) {
        const hand = splitByPokerStars[i];

        const parsedHand = analyzer.parseHand(`PokerStars ${hand}`);

        if (parsedHand) {
            const tableInfo = createTableInfo(parsedHand.info);
            const getHandfromHm = generateGetHandFromHm(tableInfo);

            const handFromHm = await hm_db.one(getHandfromHm.query, getHandfromHm.params)
                .catch(() => {
                    // console.log(err);
                    console.log(`Error: No such hand in database ${tableInfo.handid}`);
                });

            // const getHandFromHandsQuery = generateGetHandFromHandsQuery(tableInfo);
            // const handFromHands = undefined;
            // const handFromHands = await work_db.one(getHandFromHandsQuery.query, getHandFromHandsQuery.params)
            //     .catch(() => { });

            // if (handFromHands) {
            //     console.log(`Error: Duplicate hand ${tableInfo.handid}`);
            // }

            // if (handFromHm && !handFromHands) {
            if (handFromHm) {
                await handAnalyzeFile(parsedHand, hand, handFromHm, tableInfo);
                await agrAnalyzeFile(parsedHand, handFromHm, tableInfo);
            }
        }
    }
}
