
const postsRgx = /^([^:]+): posts small & big blinds [$|€]?([^ ]+)$/i;

export function calculatePosts(result, parsedHand, hand) {
    for (const post of parsedHand.posts) {
        const postIndex = result.findIndex(x => x.player === post.player);
        result[postIndex].preflop -= post.amount;
        if (parsedHand.posts.indexOf(post) > 1 && post.type === 'sb') {
            result[postIndex].isPostsSbAndBB = true;
        }
    }

    const lines = hand.split('\n');

    for (const line of lines) {
        const postsResult = line.match(postsRgx);
        if (postsResult !== null) {
            const postIndex = result.findIndex(x => x.player === postsResult[1]);
            if (postIndex !== -1) {
                result[postIndex].preflop -= postsResult[2];
                result[postIndex].isPostsSbAndBB = true;
            }
        }
    }

    return result;
}

export function calculateCap(result, parsedHand, hand) {
    let capSplit = hand.split(' cap\r\n');
    if (capSplit.length === 1) {
        capSplit = hand.split(' cap\n');
    }
    for (let j = 0; j < capSplit.length - 1; j += 1) {
        const capBlock = capSplit[j];
        let ls = capBlock.split('\r\n');
        if (ls.length === 1) {
            ls = capBlock.split('\n');
        }
        const capLine = ls.slice(-1)[0];
        const words = capLine.split(/ bets | calls | raises | posts big blind /);

        const capNick = words[0].substring(0, words[0].length - 1);
        const capIndex = result.findIndex(x => capNick.indexOf(x.player) !== -1);
        if (!words[1]) {
            console.log('Error: cant find player in cap hand');
            continue; // eslint-disable-line
        }

        const chipsBefore = parsedHand.seats.find(x => x.player === capNick).chips;

        const capSplitString = words[1].split(' ');
        let capAmount = capSplitString[capSplitString.length - 1].substring(1);

        if (chipsBefore < capAmount) {
            capAmount = chipsBefore;
        }
        if (capIndex !== -1) {
            result[capIndex].cap = -capAmount;
            result[capIndex].isCap = true;
            result[capIndex].preflop = 0;

            if (result[capIndex].isPostsSbAndBB) {
                result[capIndex].cap -= parsedHand.info.sb;
            }
        }
    }
    return result;
}

export function calculateRound(result, round, roundMark, sb, ante = 0) {
    for (const roundTurn of round) {
        switch (roundTurn.type) {
            case 'bet': {
                const raiseIndex = result.findIndex(x => x.player === roundTurn.player);
                if (raiseIndex === -1) {
                    console.log('raiseError');
                }
                if (raiseIndex !== -1) {
                    if (!result[raiseIndex].isCap) {
                        result[raiseIndex][roundMark] = -roundTurn.amount;
                    }
                    // result[raiseIndex][roundMark] = -roundTurn.amount;
                }

                break;
            }
            case 'fold':
                break;
            case 'raise': {
                const raiseIndex = result.findIndex(x => x.player === roundTurn.player);
                if (raiseIndex === -1) {
                    console.log('raiseError');
                }
                if (raiseIndex !== -1) {
                    if (!result[raiseIndex].isCap) {
                        if (roundMark === 'preflop' && result[raiseIndex].isPostsSbAndBB) {
                            result[raiseIndex][roundMark] = -roundTurn.raiseTo - sb - ante;
                        } else {
                            result[raiseIndex][roundMark] = -roundTurn.raiseTo - ante;
                        }
                    }
                    // result[raiseIndex][roundMark] = -roundTurn.raiseTo;
                }
                break;
            }
            case 'call': {
                const callIndex = result.findIndex(x => x.player === roundTurn.player);
                if (callIndex === -1) {
                    console.log('callError');
                }
                if (callIndex !== -1) {
                    if (!result[callIndex].isCap) {
                        result[callIndex][roundMark] -= roundTurn.amount;
                    }
                    // result[callIndex][roundMark] -= roundTurn.amount;
                }
                break;
            }
            case 'bet-returned': {
                const returnedIndex = result.findIndex(x => x.player === roundTurn.player);
                if (returnedIndex === -1) {
                    console.log('returnError');
                }
                if (returnedIndex !== -1) {
                    result[returnedIndex][roundMark] += roundTurn.amount;
                }
                break;
            }

            default:
        }
    }
    return result;
}

export function calculateSummary(result, showdownInfo) {
    for (const showdownTurn of showdownInfo) {
        switch (showdownTurn.type) {
            case 'showed': {
                const collectIndex = result.findIndex(x => showdownTurn.player.indexOf(x.player) !== -1);
                if (showdownTurn.won) {
                    if (collectIndex === -1) {
                        console.log('collectError');
                    }
                    if (collectIndex !== -1) {
                        result[collectIndex].collect = showdownTurn.amount;
                    }
                }
                if (showdownTurn.description.indexOf('won') !== -1) {
                    const regExp = /(\d+\.?\d*)/;
                    const value = showdownTurn.description.match(regExp);
                    result[collectIndex].collect += parseFloat(value[0]);
                }
                break;
            }
            case 'collected': {
                const collectIndex = result.findIndex(x => showdownTurn.player.indexOf(x.player) !== -1);
                if (collectIndex === -1) {
                    console.log('collectError');
                }
                if (collectIndex !== -1) {
                    result[collectIndex].collect = showdownTurn.amount;
                }

                break;
            }
            default:
        }
    }
    return result;
}

export function calculateOtherRooms(result, parsedHand, hand) {
    const filtered = parsedHand.summary.filter(x => x.metadata.raw.indexOf('won') !== -1 || x.metadata.raw.indexOf('collected') !== -1);
    if (process.env.ROOM === '888' || filtered.length === 0) {
        const summary = hand.split('*** SUMMARY ***')[1].split('\n');
        for (const line of summary) {
            if (line.indexOf('won') !== -1 && line.indexOf('showed') !== -1) {
                const playerIndex = result.findIndex(x => line.indexOf(x.player) !== -1);
                const split = line.split(' ');
                const sum = split[split.length - 1].slice(2, split[split.length - 1].length - 1);

                if (playerIndex !== -1) {
                    result[playerIndex].collect = +sum;
                }
            }
        }
    }

    return result;
}

export function createTableInfo(info) {
    const date = new Date(info.year, info.month - 1, info.day, info.hour, info.min, info.sec);
    return {
        date,
        pokertype: info.pokertype,
        sb: info.sb,
        bb: info.bb,
        handid: info.handid,
    };
}

export function createPlayerResults(result) {
    return result.map(x => ({
        player: x.player,
        res: x.collect + x.preflop + x.flop + x.turn + x.river + x.cap,
    }));
}
