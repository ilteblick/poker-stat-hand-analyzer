import dateFormat from 'dateformat';

import {
    calculatePosts, calculateCap, calculateRound, calculateSummary,
    calculateOtherRooms, createPlayerResults,
} from './utils';
import {
    generateInsertIntoHandsQuery,
    generateInsertInPlayerResultsQuery,
} from './queries';
import { work_db } from '../../dbUtils/createConnections';

export async function handAnalyzeFile(parsedHand, hand, handFromHm, tableInfo) {
    // if (parsedHand.info.handid !== '172898832293') {
    //     continue;
    // }

    let result = parsedHand.seats.map(x => ({
        player: x.player,
        preflop: 0,
        flop: 0,
        turn: 0,
        river: 0,
        collect: 0,
        cap: 0,
        isCap: false,
        isPostsSbAndBB: false, // this is when make cap
    }));

    result = calculatePosts(result, parsedHand, hand);
    result = calculateCap(result, parsedHand, hand);

    result = calculateRound(result, parsedHand.preflop, 'preflop', parsedHand.info.sb, parsedHand.info.ante);
    result = calculateRound(result, parsedHand.flop, 'flop');
    result = calculateRound(result, parsedHand.turn, 'turn');
    result = calculateRound(result, parsedHand.river, 'river');

    result = calculateSummary(result, parsedHand.summary);

    result = calculateOtherRooms(result, parsedHand, hand);

    const playerResults = createPlayerResults(result);

    const date = tableInfo.date.getTime();
    const formated = dateFormat(tableInfo.date, 'yyyymm');

    if (handFromHm) {
        await work_db.tx(async (t) => {
            const insertIntoHandQuery = generateInsertIntoHandsQuery(handFromHm, date, formated, tableInfo);

            const insertQueries = playerResults.map((x) => {
                const insertIntoPlayerResultsQuery = generateInsertInPlayerResultsQuery(x, tableInfo);
                return t.none(insertIntoPlayerResultsQuery.query, insertIntoPlayerResultsQuery.params);
            });

            insertQueries.push(t.none(insertIntoHandQuery.query, insertIntoHandQuery.params));

            t.batch(insertQueries)
                .catch((err) => {
                    console.log(err);
                    console.log(`Error: Duplicate hand ${tableInfo.handid}`);
                });
        });
    }
}
