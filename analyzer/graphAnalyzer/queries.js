export function generateGetHandFromHm(tableInfo) {
    const query = `
        select * from handhistories
        join gametypes on handhistories.gametype_id = gametypes.gametype_id
        where gamenumber = $1
    `;

    return {
        query,
        params: [tableInfo.handid],
    };
}

export function generateInsertInPlayerResultsQuery(playerResult, tableInfo) {
    const query = 'insert into playerresults (playername, pokersite_id, hand_id, result) values ($1, $2, $3, $4)';

    return {
        query,
        params: [
            playerResult.player, 2, tableInfo.handid, playerResult.res,
        ],
    };
}

export function generateInsertIntoHandsQuery(handFromDb, date, formated, tableInfo) {
    const query = `
        insert into hands
            (hand_id, date, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize)
            values ($1, $2, $3, $4, $5, $6, $7, $8)
    `;

    const params = [tableInfo.handid, date, formated, handFromDb.currencytype_id, handFromDb.pokergametype_id, handFromDb.pokersite_id,
    handFromDb.bigblindincents, handFromDb.tablesize];

    return {
        query,
        params,
    };
}

export function generateFormatPlayerResultsQuery() {
    return {
        query: `
            INSERT INTO formatedplayerresults
            (playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id)(
            select playername, result, hm_format_date, currencytype_id, pokergametype_id,
                pokersite_id, bigblindincents, tablesize, hand_id from (
        select playername, cast (rnum / 100 as int) as num, sum(result) as result, hm_format_date,
                currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize,  min(hand_id) as hand_id  from (
        select *, row_number() OVER () as rnum from (
        select playername, result, hm_format_date, currencytype_id, pokergametype_id, playerresults.pokersite_id, playerresults.hand_id,
                bigblindincents, tablesize from playerresults
        join hands on playerresults.hand_id = hands.hand_id order by playername, date ) as a) as b
        group by num, playername, hm_format_date, currencytype_id, pokergametype_id, pokersite_id,
                bigblindincents, tablesize )as c)
        `,
    };
}

export function generateGetHandFromHandsQuery(tableInfo) {
    return {
        query: 'select * from hands where hand_id = $1',
        params: [tableInfo.handid],
    };
}
